### 1. [Pre-Requirements](./README.md)
### 2. [Install Windows VM](./VmInstall.md)
### 3. [VFIO](./VFIO.MD)
## 4. [Looking Glass](./LookingGlass.md)
### 5. [Tips And Fixes](./TipsAndFixes.md)

Add `<shmem...` xml at the end of devices.
``` xml
<devices>
    ...
    <shmem name='looking-glass'>
        <model type='ivshmem-plain'/>
        <size unit='M'>32</size>
    </shmem>
</devices>
```
`size = (width * height * 4 * 2) / 1024 / 1024 - 10`

and size also needs to be power of 2 (2, 4, 8, 16...), in my case 32MB for resolution 1920x1200.

## IVSHEM driver
You'll need to install [IVSHEM driver](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/upstream-virtio/)

![](./images/ivshem-update.png)

![](./images/ivshem-browse.png)

![](./images/ivshem-driver.png)

After driver installation the device should look like this

![](./images/ivshem-device.png)

## Install looking glass
First choose your version on [looking glass site](https://looking-glass.io/downloads)
### Guest
Install windows host binary and install it. Don't worry if it won't start, we'll have to remove qxl device first.
### Host
Download source file of the same version on your host and extract it.

Go into client directory with terminal and use commands `cmake ./` and `make`. After that you'll get linux client binary that you'll use to launch looking glass.

## Get it working
Now that you have all drivers installer and you have looking-glass-client, we can get it working. You'll need to edit video QXL xml from `type="qxl"` to `type="none"`. After that looking glass should work. You'll just need to boot into windows and use looking-glass-client binary.

And finally we have working VM with looking glass
![](./images/looking-glass.png)

## [Continue to Tips And Fixes](./TipsAndFixes.md)