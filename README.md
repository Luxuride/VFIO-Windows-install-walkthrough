# VFIO windows install walkthrough using VirtManager and looking glass
This walkthrough is not fully describing windows installation in VirtManager. It's only pointing on optimisations and VFIO oriented problems. It's assumed that reader is able to make basic windows installation.
## 1. [Pre-Requirements](./README.md)
### 2. [Install Windows VM](./VmInstall.md)
### 3. [VFIO](./VFIO.MD)
### 4. [Looking Glass](./LookingGlass.md)
### 5. [Tips And Fixes](./TipsAndFixes.md)

## My Hardware
### CPU
`AMD Ryzen 5 5600X (12) @ 3.700GHz`
### Host GPU
`GPU: AMD ATI Radeon 540/540X/550/550X / RX 540X/550/550X`
### Guest GPU
`GPU: AMD ATI Radeon HD 6850`
## Pre-requirements
### Enable IOMMU
Set `intel_iommu=on` or `amd_iommu=on` respective to your system in grub config

Set `iommu=pt` in grub config for safety reasons

## [Continue to VM installation](./VmInstall.md)