### 1. [Pre-Requirements](./README.md)
### 2. [Install Windows VM](./VmInstall.md)
### 3. [VFIO](./VFIO.MD)
### 4. [Looking Glass](./LookingGlass.md)
## 5. [Tips And Fixes](./TipsAndFixes.md)

# Tips
## Top video performance + ease of use
From my experience I've found that using looking glass only as VM input is also great option. You can set in looking glass config that you don't want to use video or use shortcut ScrLk+v to disable video. Then if you enable capture and switch your monitor input you get full video performance but you don't need to manage your input devices, which is from my experience the most annoying thing with those VM setups.
# Fixes
## My sound is not working
If you don't need your linux host sound you can pass your motherboard sound controller via PCI

If you want sound from both linux and windows change your sound xml from
``` xml
<sound model="ich9">
  <address type="pci" domain="0x0000" bus="0x00" slot="0x1b" function="0x0"/>
</sound>
```
to
``` xml
<sound model="ich9">
  <codec type="micro"/>
  <audio id="1"/>
  <address type="pci" domain="0x0000" bus="0x00" slot="0x1b" function="0x0"/>
</sound>
```
## My GPU is not disabled
Maybe your kernel is not built with vfio-pci support. You could try linux-zen kernel.
## My mouse is jumping in looking glass
Disable windows mouse acceleration
## How do I use mouse and keyboard with hdmi switching
You pass them as usb device. *You can't controll your host system with those after passing them through*.
![](./images/usb-pass.png)

## [Back to readme](./README.md)