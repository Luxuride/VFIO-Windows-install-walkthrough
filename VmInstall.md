### 1. [Pre-Requirements](./README.md)
## 2. [Install Windows VM](./VmInstall.md)
### 3. [VFIO](./VFIO.MD)
### 4. [Looking Glass](./LookingGlass.md)
### 5. [Tips And Fixes](./TipsAndFixes.md)
Download your windows 10 iso [here](https://www.microsoft.com/en-us/software-download/windows10ISO) or use your existing iso.

Also download virtio drivers [here](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/virtio-win.iso) or [choose your iso](https://pve.proxmox.com/wiki/Windows_VirtIO_Drivers#Using_the_ISO). We'll be using those to get some extra performance and integration with looking glass.

Before start of the installation select **Customize configuration before install**

![Check Customize configuration](./images/win-preinstall.png)

## Configuration before installation
![](./images/check-bios.png)
![](./images/virtio-disk.png)
![](./images/driver-iso.png)
![](./images/virtio-network.png)

## Load virtio drivers in installation
![](./images/load-driver.png)
![](./images/browse-driver.png)
![](./images/choose-driver.png)
![](./images/finish-driver.png)
After that you can continue with normal windows installation

## Installing virtio drivers
Install drivers from the CD drive we were adding before installation. **virtio-win-guest-tools** should install all drivers.
![](./images/virtio-install.png)

## [Continue to VFIO](./VFIO.MD)